'use strict'

// required by webpack
// listing these files here is necessary, since webpack won't find them otherwise
require("./styles.scss")
require("./js/fonts")


// import {init as initGl} from './js/arabesque-background'
// import {init as initGl} from './js/attractor/lorenz-attractor'
import {init as initGl} from './js/particles/particles'

function initialize() {
    initGl(document.getElementById('gl'), window.innerWidth, window.innerHeight)
}

function ready(fn) {
    if (document.readyState != 'loading'){
       fn()
    } else {
      document.addEventListener('DOMContentLoaded', fn);
    }
  }
  

  ready(initialize)
