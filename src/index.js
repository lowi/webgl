'use strict'

// required by webpack
// listing these files here is necessary, since webpack won't find them otherwise
require("./styles.scss")
require("./js/fonts")

// import {init as initGl} from './js/arabesque/main'
// import {init as initGl} from './js/particles/main'
// import {init as initGl} from './js/multi-pass/main'
import {init as initGl} from './js/attractor/main'

function initialize() {
    let glContainer = document.getElementById('gl')
    let guiContainer = document.getElementById('gl-gui')

    initBulma()
    initGl(glContainer, guiContainer, window.innerWidth, window.innerHeight)

}

function ready(fn) {
    if (document.readyState != 'loading'){
       fn()
    } else {
      document.addEventListener('DOMContentLoaded', fn);
    }
  }
  ready(initialize)



function initBulma() {
  console.log("init bulma")


  // Initialize Navigation panel
  // lifted from : https://bulma.io/documentation/components/navbar/
  // ———————————————————————————————————————————————————————————————

  // Get all "navbar-burger" elements
  const navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

  // Check if there are any navbar burgers
  if (navbarBurgers.length > 0) {

    // Add a click event on each of them
    navbarBurgers.forEach( el => {
      el.addEventListener('click', () => {
        // Get the target from the "data-target" attribute
        const target = document.getElementById(el.dataset.target);

        // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
        el.classList.toggle('is-active');
        target.classList.toggle('is-active');
      });
    });
  }
}