import * as THREE from 'three'
import _ from 'lodash'
import * as GPUSim from '../common/gpu-sim'
import * as SimulationParameters from './params'

export function initialize(textureDimensions, initialDataTextures, shaders, helper) {
    
    console.log("initialize simulation")

    const parameters = SimulationParameters.base()

    const velocityShader = setupVelocityShader( shaders, initialDataTextures, parameters, 1 )
	const velocity = GPUSim.initialize( textureDimensions, velocityShader )
	helper.attach( velocity.frameBuffers[ 0 ], 'Velocity FBO#0' )
    helper.attach( velocity.frameBuffers[ 1 ], 'Velocity FBO#1' )

    const positionShader = setupPositionShader(shaders, initialDataTextures, parameters)
	const position = GPUSim.initialize( textureDimensions, positionShader )

	helper.attach( position.frameBuffers[ 0 ], 'Position FBO#0' )
    helper.attach( position.frameBuffers[ 1 ], 'Position FBO#1' )

	
	const simulation = {velocity, position, parameters}
	// const simulation = {position, parameters}
	
	// attach update function
	simulation.update = update
	
    return simulation
   
}


function update(time, renderer) {

	// update the shader uniforms
	this.velocity.shader.uniforms.time.value = time
	
	// render the velocity
	this.velocity.render(renderer)

	// feed the result of our velocity simulation into the position simulation
    this.position.shader.uniforms.velocityTexture.value = this.velocity.output
	
	// so we can render the new positions
	this.position.render(renderer)
	
	// finally we prepare for the next time around, by updating the positions in the velocity simulation 
    this.velocity.shader.uniforms.positionTexture.value = this.position.output
}



function setupPositionShader(shaders, initialDataTextures, parameters) {
	return new THREE.RawShaderMaterial( {
		uniforms:{
			// baseline information:

			// random variation added to the initial position
			spread: { type: 'f', value: parameters.spread },
			
			// during init and whenever a particle dies, it gets (re-)spawned here 
			originalTexture: { type: 't', value: initialDataTextures.positions },
			
			// just some variation between the pixels, we can sprinkle here and there to achieve some randomness
            infoTexture: { type: 't', value: initialDataTextures.variations },

			
			// inputs:

			// position information
			// which gets written during gpuSimulation.update
			inputTexture: { type: 't', value: null },

			// the current velocity of each pixel
			// gets updated during update()
            velocityTexture: { type: 't', value: null }
		},

		vertexShader: shaders.vertex.ortho,
		fragmentShader: shaders.fragment.position,
		depthTest: false,
		depthWrite: false
	} )
}

function setupVelocityShader(shaders, initialDataTextures, parameters, numSpheres) {
    
	return new THREE.RawShaderMaterial( {
		uniforms:{
			time: { type: 'f', value: 0 },
			bounce: { type: 'f', value: parameters.bounce },
			friction: { type: 'f', value: parameters.friction },
			gravity: { type: 'f', value: parameters.gravity },
			pressure: { type: 'f', value: parameters.pressure },
			infoTexture: { type: 't', value: initialDataTextures.variations },
			originalTexture: { type: 't', value: initialDataTextures.positions },
			positionTexture: { type: 't', value: initialDataTextures.positions },
			inputTexture: { type: 't', value: null }
		},
        vertexShader: shaders.vertex.ortho,
		fragmentShader: shaders.fragment.velocity,
		depthTest: false,
		depthWrite: false
	} )
}


function zeros(count) {
    return _().range(count).map((i) => new THREE.Vector4( 0, 0, 0, 0 ) ).value()
}

