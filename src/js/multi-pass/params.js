
export function base() {
	
	const bounce = 4;
	const friction = .4;
	const pressure = 0.;
	const spread = .1;
	const gravity = .4;
	const size = 1;
	const useTriangles = false;
	const showSpheres = true;

	return { bounce, friction, pressure, spread, gravity, size, useTriangles, showSpheres }
	
}