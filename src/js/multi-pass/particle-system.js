
import * as THREE from 'three'

import particleVert from '../../../assets/shaders/multi-pass/particle.vert'
import particleFrag from '../../../assets/shaders/multi-pass/particle.frag'


export function initialize(dimensions, initialDataTextures, simulationParameters) {

    if(dimensions.x != dimensions.y) console.warn("Obacht! simulation dimensions are not square.", dimensions)

    const particleGeometry = new THREE.BufferGeometry();
    const particles = dimensions.x * dimensions.y * 3;
    const positions = new Float32Array( particles * 3 );
    const uvs = new Float32Array( particles * 2 );
    const ids = new Float32Array( particles * 2 );

    let h = Math.sqrt( 1 * 1 - .5 * .5 );
    let points = [
        new THREE.Vector3( -.5, - .5 * h ),
        new THREE.Vector3( 0, .5 * h ),
        new THREE.Vector3( .5, - .5 * h )
    ];
    let coords = [
        new THREE.Vector2( 0, 1 ),
        new THREE.Vector2( .5, 0 ),
        new THREE.Vector2( 1, 1 )
    ];

    for ( let i = 0, i2 = 0; i < particles * 3; i += 3, i2++ ) {
        positions[ i + 0 ] = points[ i2 % 3 ].x;
        positions[ i + 1 ] = points[ i2 % 3 ].y;
        positions[ i + 2 ] = points[ i2 % 3 ].z;
    }

    for ( let i = 0, i2 = 0; i < particles * 2; i += 2, i2++ ) {
        uvs[ i + 0 ] = coords[ i2 % 3 ].x;
        uvs[ i + 1 ] = coords[ i2 % 3 ].y;
    }

    for ( let i = 0, i2 = 0; i < particles * 2; i += 6, i2++ ) {
        ids[ i + 0 ] = i2;
        ids[ i + 1 ] = i2;
        ids[ i + 2 ] = i2;
        ids[ i + 3 ] = i2;
        ids[ i + 4 ] = i2;
        ids[ i + 5 ] = i2;
    }

    particleGeometry.setAttribute( 'position', new THREE.BufferAttribute( positions, 3 ) );
    particleGeometry.setAttribute( 'uv', new THREE.BufferAttribute( uvs, 2 ) );
    particleGeometry.setAttribute( 'id', new THREE.BufferAttribute( ids, 2 ) );


    let particleMaterial = new THREE.RawShaderMaterial( {
        uniforms:{

            // the size of the particles
            size: { type: 'f', value: simulationParameters.size },

            // the starting positions
            originalTexture: { type: 't', value: initialDataTextures.positions },

            // some variability for each particle
            infoTexture: { type: 't', value: initialDataTextures.variations },
            
            // position and velocity textures are set during update()
            positionTexture: { type: 't', value: null },
            previousPositionTexture: { type: 't', value: null },
            velocityTexture: { type: 't', value: null },
            
            // the size of our simulation (square 2-exponent, eg. 32, 128, 512)
            dimensions: { type: 'v2', value: dimensions },

            cameraPosition: { type: 'v3', value: new THREE.Vector3() },
            prevModelViewMatrix: { type: 'm4', value: new THREE.Matrix4() },
            prevProjectionMatrix: { type: 'm4', value: new THREE.Matrix4() }

        },

        vertexShader: particleVert,
        fragmentShader: particleFrag,
        transparent: false,
        side: THREE.DoubleSide,
        depthTest: true,
        depthWrite: true
    } );

    const mesh = new THREE.Mesh( particleGeometry, particleMaterial )

    // supply an update function
    mesh.updateSimulation = (simulation) => {

        let position = simulation.position.output 
        let previousPosition = simulation.position.input 
        let velocity = simulation.velocity.output 

        mesh.material.uniforms.positionTexture.value = position
        mesh.material.uniforms.previousPositionTexture.value = previousPosition
        mesh.material.uniforms.velocityTexture.value = velocity
    }  

    mesh.updateCamera = (camera) => {
        mesh.material.uniforms.cameraPosition.value.copy( camera.position );
        mesh.material.uniforms.prevModelViewMatrix.value.multiplyMatrices( camera.matrixWorldInverse, mesh.matrixWorld );
        mesh.material.uniforms.prevProjectionMatrix.value.copy( camera.projectionMatrix );
    }
        
    return mesh
}

