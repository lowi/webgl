
import * as THREE from 'three'
import _ from 'lodash'
import * as FBOHelper from 'three.fbo-helper'

import * as Shaders from './shader-handler'
import * as Screen from '../common/output-render-target'
import * as Tableau from './tableau'

console.log("Particle rain")

// load the shaders
const NUM_SPHERES = 1
let shaders = Shaders.initialize(NUM_SPHERES)


// our renderer
const renderer = new THREE.WebGLRenderer( { antialias: true, alpha: false } )

let tableau


// helper for debugging FrameBufferObjects
// https://github.com/spite/THREE.FBOHelper
const helper = new FBOHelper( renderer )


// Render targets
// ——————————————

const motionRT = createRenderTarget( 1, 1 ) 

const screenOut = Screen.initialize( shaders.fragment.final)


export function init(container, width, height) {

    // attach to DOM
    container.appendChild( renderer.domElement )
    // todo: make window.devicePixelRatio a parameter
	renderer.setPixelRatio(window.devicePixelRatio ? window.devicePixelRatio : 1)
 
    // const textureDimensions = new THREE.Vector2( 512, 512 )
    // const textureDimensions = new THREE.Vector2( 8, 8 )
    const textureDimensions = new THREE.Vector2( 64, 64 )
    const windowDimensions = new THREE.Vector2( width, height )
    

    tableau = Tableau.initialize(shaders, textureDimensions, windowDimensions, helper)

    
    // helper.attach( motionRT, 'Motion', function( i ) { return 'Speed: (' + i.r + ', ' + i.g + ')<br/>Life: ' + i.b + ' dLife: ' + i.a } );
    
    
    window.addEventListener( 'resize', onWindowResized )
    onWindowResized()

    // Here we go…
    // ———————————
    animate()
}


function createRenderTarget( width, height ) {
	var rt = new THREE.WebGLRenderTarget( width, height, {
		wrapS: THREE.ClampToEdgeWrapping,
		wrapT: THREE.ClampToEdgeWrapping,
		format: THREE.RGBAFormat,
		// type: isMobile.any ? THREE.HalfFloatType : THREE.FloatType,
		type: THREE.FloatType,
		stencilBuffer: false,
		depthBuffer: true
	});
	rt.flipY = true;
	return rt;
}



function onWindowResized( event ) {
    var w = window.innerWidth
    var h = window.innerHeight
    const windowDimensions =  new THREE.Vector2( window.innerWidth, window.innerHeight )

    // console.log("Window resized:", w, h)

    renderer.setSize( w, h )
    helper.setSize( w, h )
    motionRT.setSize( w, h )

    tableau.resize(windowDimensions)
    screenOut.resize(windowDimensions)

    helper.refreshFBO( motionRT )
}

function animate() {
    requestAnimationFrame( animate )
    render()
}

let time = 0
let lastTime = 0

function render() {

    // update the time
    time += .0005 * ( performance.now() - lastTime )

    tableau.update(time, renderer, motionRT)

    // and now we render the result to our final render target
    screenOut.render(renderer, time, motionRT)
   
    helper.update()

    lastTime = performance.now()
}