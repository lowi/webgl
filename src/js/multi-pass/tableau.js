import * as THREE from 'three'
import _ from 'lodash'

import * as ParticleSystem from './particle-system'
import * as Simulation from './simulation'
import {generateInitialData} from './initial-data'

export function initialize(
    shaders,
    simulationDimensions, 
    windowDimensions,
    helper) {


    const initialDataTextures = generateInitialData( simulationDimensions )

    const simulation = Simulation.initialize(simulationDimensions, initialDataTextures, shaders, helper)
    

    const scene = new THREE.Scene()

    const camera = new THREE.PerspectiveCamera( 60, 1, .01, 10000 )
    camera.target = new THREE.Vector3( 0, 0, 0 );
    camera.position.set( 0, 240, 3600 );
    camera.aspect = windowDimensions.x / windowDimensions.y

    // scene objects
    const world =  new THREE.Object3D();
    
    // const light = new THREE.HemisphereLight( 0xffffbb, 0x888888, 1 )
    const light = new THREE.PointLight( 0xffff00, 100000 )

    // particle system
    const particleSystem = ParticleSystem.initialize(simulationDimensions, initialDataTextures, simulation.parameters)


    // assemble
    world.add( particleSystem )
    scene.add( light )
    scene.add( camera )
    scene.add( world )

    const tableau = { scene, camera, world, particleSystem, simulation }

    // attach update and resize functions
    tableau.update = update
    tableau.resize = resize

    return tableau
}

function update(time, renderer, renderTarget) {

    this.simulation.update(time, renderer)

    // also rotate the world
    // this.world.rotation.y = .1 * time

    // now we feed the result of the simulation into the particle shader
    this.particleSystem.updateSimulation(this.simulation)
    this.particleSystem.updateCamera(this.camera)

    renderer.setRenderTarget(renderTarget)
    renderer.render( this.scene, this.camera)
    
}


function resize(windowDimensions) {
    this.camera.aspect = windowDimensions.x / windowDimensions.y
    this.camera.updateProjectionMatrix()
}