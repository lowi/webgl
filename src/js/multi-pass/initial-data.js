
import * as THREE from 'three'
import { Maf } from '../common/maf'

export function generateInitialData(dimensions) {

	var ptr = 0;

	var positions = new Float32Array( dimensions.x * dimensions.y * 4 );
	var variations = new Float32Array( dimensions.x * dimensions.y * 4 );

	for( var y = 0; y < dimensions.y; y++ ) {
		for( var x = 0; x < dimensions.x; x++ ) {

			var a = Maf.randomInRange( 0, 1 );
			var b = Maf.randomInRange( 0, 1 );
			if( a > b ) {
				var tmp = b;
				b = a;
				a = tmp;
			}
			var px = b * 400 * Math.cos( 2 * Math.PI * a / b );
			var pz = b * 400 * Math.sin( 2 * Math.PI * a / b );

			positions[ ptr ] = px

			// string them up in a column pointing upwards
			positions[ ptr + 1 ] = Maf.randomInRange( 100, 4000 );
			
			positions[ ptr + 2 ] = pz;
			positions[ ptr + 3 ] = Maf.randomInRange( 1, 499 );

			// console.log("positions:", positions[ ptr ], positions[ ptr +1], positions[ ptr +2], positions[ ptr +3] )

			variations[ ptr ] = Maf.randomInRange( 0.1, 2 );
			// variations[ ptr ] = 1;

			// console.log("variations:", variations[ ptr ], variations[ ptr +1], variations[ ptr +2], variations[ ptr +3] )

			ptr += 4;
		}
	}

	// textures
	const positionsTexture = new THREE.DataTexture( positions, dimensions.x, dimensions.y, THREE.RGBAFormat, THREE.FloatType );
	positionsTexture.minFilter = THREE.NearestFilter;
	positionsTexture.magFilter = THREE.NearestFilter;
	positionsTexture.needsUpdate = true;

	const variationTexture = new THREE.DataTexture( variations, dimensions.x, dimensions.y, THREE.RGBAFormat, THREE.FloatType );
	variationTexture.minFilter = THREE.NearestFilter;
	variationTexture.magFilter = THREE.NearestFilter;
	variationTexture.needsUpdate = true;

	// return [positionsTexture, variationTexture]
	return {positions: positionsTexture, variations: variationTexture}
}
