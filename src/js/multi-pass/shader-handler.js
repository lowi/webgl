
import * as Preprocessor from "../common/shader-preprocessor"

import objectVert from '../../../assets/shaders/multi-pass/object.vert'
import objectFrag from '../../../assets/shaders/multi-pass/object.frag'

import orthoVert from '../../../assets/shaders/common/ortho.vert'
import velocityFragRaw from '../../../assets/shaders/multi-pass/velocity.frag'
import positionFrag from '../../../assets/shaders/multi-pass/position.frag'
import finalFragRaw from '../../../assets/shaders/multi-pass/final.frag'




export function initialize(numSpheres) {

    // some of the shaders require a bit of preprocessing
    const velocityFrag = Preprocessor.prepare(velocityFragRaw, 
        new Map([["NUM_SPHERES", numSpheres]]))

    const finalFrag = Preprocessor.prepare(finalFragRaw, 
        new Map([["NUM_SPHERES", numSpheres]]))

     return {
         vertex: {
             object: objectVert,
             ortho: orthoVert
         },
         fragment: {
            object: objectFrag,
            velocity: velocityFrag,
            position: positionFrag,
            final: finalFrag
         }
     }
}