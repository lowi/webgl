import * as THREE from 'three'
import _ from 'lodash'

import * as ParticleSystem from './particle-system'
import * as Simulation from './simulation'
import {generateInitialData} from './initial-data'

export function initialize(
    shaders,
    simulationDimensions, 
    windowDimensions,
    floatType,
    helper) {

	
    const initialState = generateInitialData( simulationDimensions, floatType )
    
    const simulation = Simulation.initialize(floatType, simulationDimensions, shaders, initialState, helper)
    const scene = new THREE.Scene()

    const camera = new THREE.PerspectiveCamera( 10, 1, 0.1, 100000 )
    camera.target = new THREE.Vector3( 0, 0, 0 );
    camera.position.set( 0, 0, 22000 );
    camera.aspect = windowDimensions.x / windowDimensions.y

    // scene objects
    const world =  new THREE.Object3D();
    world.rotation.y = _.random(-3., 3.);
    
    // const light = new THREE.HemisphereLight( 0xffffbb, 0x888888, 1 )
    const light = new THREE.PointLight( 0xffff00, 100 )

    // particle system
    const particleSystem = ParticleSystem.initialize(simulationDimensions, initialState.positions)

    // assemble
    world.add( particleSystem )
    // scene.add( light )
    scene.add( world )

    const tableau = { scene, camera, world, particleSystem, simulation }

    // attach update and resize functions
    tableau.update = update
    tableau.resize = resize

    return tableau
}

const backgroundColor = new THREE.Color('white')

function update(time, delta, renderer, renderTarget) {

    // strangely enough, by toggling renderer.setClearColor() on or off we can invert the render 
    // how cool is that!?
    // I have no idea how this works
    renderer.setClearColor(backgroundColor);

    let t = this.simulation.update(delta, renderer)

    this.world.rotation.y = .00004 * delta


    // now we feed the result of the simulation into the particle system
    this.particleSystem.updateSimulation( t)
    // this.particleSystem.updateCamera(this.camera)

    renderer.setRenderTarget(renderTarget)
    renderer.render( this.scene, this.camera)
    
}


function resize(windowDimensions) {
    this.camera.aspect = windowDimensions.x / windowDimensions.y
    this.camera.updateProjectionMatrix()
}

