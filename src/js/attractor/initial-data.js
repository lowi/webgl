
import * as THREE from 'three'
import { Maf } from '../common/maf'

const POSITION_SCALE = 0.1
const randomPosition = () => POSITION_SCALE * (Math.random() + Math.random() + Math.random() - 1.5)

function randomPositions(dimensions) { 
	return _().range(dimensions.x * dimensions.y)
		// .map(() => [randomPosition(), randomPosition(), randomPosition()+25, 0])
		.map(() => [randomPosition(), randomPosition(), randomPosition(), 0])
		.flatten()
		.value()
}

function randomVariations(dimensions){ 
	return _().range(dimensions.x * dimensions.y)
		.map(() => [ Maf.randomInRange( 0.1, 2 ), 0, 0, 0])
		.flatten()
		.value()
}


export function generateInitialData(dimensions, floatType) {

	let positions = new Float32Array(randomPositions(dimensions))
	let variations = new Float32Array( randomVariations(dimensions) )

	// textures
	const positionsTexture = new THREE.DataTexture( positions, dimensions.x, dimensions.y, THREE.RGBAFormat, floatType )
	positionsTexture.minFilter = THREE.NearestFilter
	positionsTexture.magFilter = THREE.NearestFilter
	positionsTexture.needsUpdate = true

	const variationTexture = new THREE.DataTexture( variations, dimensions.x, dimensions.y, THREE.RGBAFormat, floatType )
	variationTexture.minFilter = THREE.NearestFilter
	variationTexture.magFilter = THREE.NearestFilter
	variationTexture.needsUpdate = true

	return {positions: positionsTexture, variations: variationTexture}
}
