import * as THREE from 'three'
import _ from 'lodash'
import { Vector4 } from 'three'

export function initialize(floatType, dimensions, shaders, initialState, helper) {

	const width = dimensions.x
	const height = dimensions.y
	const initialPositions = initialState.positions

	const shader = setupSimulationShader(shaders, initialPositions)
	
	const scene = new THREE.Scene()
	const camera = new THREE.OrthographicCamera( width / - 2, width / 2, height / 2 , height / - 2, .00001, 1000 ) 
	const quad = new THREE.Mesh(new THREE.PlaneBufferGeometry( width, height ), shader)
	scene.add( quad )


	// create two frameBuffers, into which we're gonna write in a tick-tock strategy	
	let fbo0 = createFBO( width, height, floatType)
	let fbo1 = createFBO( width, height, floatType)
	const frameBuffers = {in: fbo0, out: fbo1}

	// flag we use to keep track of the currently selected frameBuffer 
	let currentFramebufferIndex = 0

	// helper.attach( fbo0, 'fbo 0' )
	// helper.attach( fbo1, 'fbo 1' )

	// const attractorSimulation = GPUSim.initialize( dimensions, shader, initialState.positions )
	const simulation = { scene, camera, shader, frameBuffers, currentFramebufferIndex}
	
	simulation.update = update
	simulation.toggle = toggle

    return simulation
}

let dt = 0.0004

function update(delta, renderer) {
	// this.shader.uniforms.dt.value = delta * 0.01;

	// point the shader input texture towards the currently active framebuffer
    this.shader.uniforms.positions.value = this.frameBuffers.in
	renderer.setRenderTarget(this.frameBuffers.out)
	renderer.render( this.scene, this.camera );
	this.toggle()
	return this.frameBuffers.out
}

function toggle() {
	let currentIn = this.frameBuffers.in
	let currentOut = this.frameBuffers.out
	this.frameBuffers = {in: currentOut, out: currentIn}
}

// the current result is in the input, because the buffers get toggled right after the render
function result () {
	let result = this.frameBuffers.out
	return result
}


function createFBO( width, height, floatType) {
	var fbo = new THREE.WebGLRenderTarget( width, height, {
		wrapS: THREE.ClampToEdgeWrapping,
		wrapT: THREE.ClampToEdgeWrapping,
		minFilter: THREE.NearestFilter,
		magFilter: THREE.NearestFilter,
		format: THREE.RGBAFormat,
		type: floatType,
		stencilBuffer: false,
		depthBuffer: false
	})
	fbo.texture.generateMipmaps = false
	return fbo
}


function setupSimulationShader(shaders, initialState) {
	return new THREE.RawShaderMaterial( {
		uniforms:{
			dt: { type: 'f', value: dt},
			origin: { type: 't', value: initialState },
			positions: { type: 't', value: null },
		},
		vertexShader: shaders.vertex.ortho,
		fragmentShader: shaders.fragment.attractor,
		depthTest: false,
		depthWrite: false
	} )
}

