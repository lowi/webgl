
import * as THREE from 'three'
import _ from 'lodash'
import * as FBOHelper from 'three.fbo-helper'

import * as Shaders from './shader-handler'
import * as Screen from '../common/output-render-target'
import * as Tableau from './tableau'
import { getFloatType } from '../common/gl-check'


console.log("ATTRACTOR")

// load the shaders
const NUM_SPHERES = 1
let shaders = Shaders.initialize(NUM_SPHERES)


// our renderer
const renderer = new THREE.WebGLRenderer( { antialias: true, alpha: false } )

var gl = renderer.getContext();

//https://github.com/KhronosGroup/WebGL/blob/master/sdk/tests/conformance/extensions/oes-texture-float.html
// if (!gl.getExtension("OES_texture_float")){
//     throw new Error( "float textures not supported" );
// }
const floatType = getFloatType(renderer)


if(floatType ==  THREE.FloatType) console.log("THREE.FloatType")
else if(floatType ==  THREE.HalfFloatType) console.log("THREE.HalfFloatType")
else console.warn("unknown float type:", floatType)


//https://github.com/KhronosGroup/WebGL/blob/90ceaac0c4546b1aad634a6a5c4d2dfae9f4d124/conformance-suites/1.0.0/extra/webgl-info.html
if( gl.getParameter(gl.MAX_VERTEX_TEXTURE_IMAGE_UNITS) == 0 ) {
    throw new Error( "vertex shader cannot read textures" );
}



let tableau


// helper for debugging FrameBufferObjects
// https://github.com/spite/THREE.FBOHelper
const helper = new FBOHelper( renderer )


// Render targets
// ——————————————

const motionRT = createRenderTarget( 1, 1 ) 

const screenOut = Screen.initialize( shaders.fragment.final)


export function init(container, guiContainer, width, height) {

    // attach to DOM
    container.appendChild( renderer.domElement )
    // todo: make window.devicePixelRatio a parameter
	renderer.setPixelRatio(window.devicePixelRatio ? window.devicePixelRatio : 1)

    generateGui(guiContainer)

    const textureDimensions = new THREE.Vector2( 512, 512 )
    // const textureDimensions = new THREE.Vector2( 256, 256 )
    // const textureDimensions = new THREE.Vector2( 64, 64 )
    // const textureDimensions = new THREE.Vector2( 8, 8 )
    // const textureDimensions = new THREE.Vector2( 4, 4 )
    const windowDimensions = new THREE.Vector2( width, height )
    

    tableau = Tableau.initialize(shaders, textureDimensions, windowDimensions, floatType, helper)

    
    // helper.attach( motionRT, 'Motion', function( i ) { return 'Speed: (' + i.r + ', ' + i.g + ')<br/>Life: ' + i.b + ' dLife: ' + i.a } );
    
    
    window.addEventListener( 'resize', onWindowResized )
    onWindowResized()

    // Here we go…
    // ———————————
    animate()
}

function generateGui(container) {
}


function createRenderTarget( width, height ) {
	var rt = new THREE.WebGLRenderTarget( width, height, {
		wrapS: THREE.ClampToEdgeWrapping,
		wrapT: THREE.ClampToEdgeWrapping,
		format: THREE.RGBAFormat,
		type: floatType,
		stencilBuffer: false,
		depthBuffer: true
	});
	rt.flipY = true;
	return rt;
}



function onWindowResized( event ) {
    var w = window.innerWidth
    var h = window.innerHeight
    const windowDimensions =  new THREE.Vector2( window.innerWidth, window.innerHeight )

    // console.log("Window resized:", w, h)

    renderer.setSize( w, h )
    helper.setSize( w, h )
    motionRT.setSize( w, h )

    tableau.resize(windowDimensions)
    screenOut.resize(windowDimensions)

    helper.refreshFBO( motionRT )
}

function animate() {
    requestAnimationFrame( animate )
    render()
}

let time = 0
let lastTime = 0
let delta = 0

function render() {



    // update the time
    delta = ( performance.now() - lastTime )
    time += delta

    tableau.update(time, delta, renderer, motionRT)

    // and now we render the result to our final render target
    screenOut.render(renderer, time, motionRT)
   
    helper.update()
    framerate(delta)

    lastTime = performance.now()
}

const NUM_FRAMES = 20
let frames = 0
let sum = 0
function framerate(){
    sum += delta
    frames += 1
    if ( frames > NUM_FRAMES ) {
        // sum in ms => time it took to render 20 frames
        var fps = Math.round(1000 * NUM_FRAMES / sum)
        // console.log("framerate", fps)

        // hackedy-hack
        // todo: clean this up
        document.getElementById("framerate").innerHTML = fps

        frames = 0  
        sum = 0
    }
}