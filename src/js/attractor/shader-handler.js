
import * as Preprocessor from "../common/shader-preprocessor"

import objectVert from '../../../assets/shaders/attractor/object.vert'
import objectFrag from '../../../assets/shaders/attractor/object.frag'

import orthoVert from '../../../assets/shaders/common/ortho.vert'
import attractorFrag from '../../../assets/shaders/attractor/attractor.frag'
import finalFragRaw from '../../../assets/shaders/attractor/final.frag'




export function initialize(numSpheres) {

  
    const finalFrag = Preprocessor.prepare(finalFragRaw, 
        new Map([["NUM_SPHERES", numSpheres]]))

     return {
         vertex: {
             object: objectVert,
             ortho: orthoVert
         },
         fragment: {
            object: objectFrag,
            attractor: attractorFrag,
            final: finalFrag
         }
     }
}