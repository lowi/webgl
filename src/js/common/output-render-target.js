

import * as THREE from 'three'
import orthographicVertexShader from '../../../assets/shaders/common/ortho.vert'

export function initialize(fragmentShader) {
    
    const shader = new THREE.RawShaderMaterial( {
        uniforms:{
            time: { type: 'f', value: 0 },
            motionBuffer: { type: 't', value: null },
            dimensions: { type: 'v2', value: new THREE.Vector2( 1, 1 ) }
        },
        vertexShader: orthographicVertexShader,
        fragmentShader: fragmentShader,
        depthTest: false,
        depthWrite: false
    } );

    const scene = new THREE.Scene();
    const camera = new THREE.OrthographicCamera( 1 / - 2, 1 / 2, 1 / 2, 1 / - 2, .00001, 1000 );
    const quad = new THREE.Mesh( new THREE.PlaneBufferGeometry( 1, 1 ), shader );
    scene.add( quad )

    const composition = { scene, camera, quad, shader }
    composition.render = render
    composition.resize = resize

    return composition
}

function render(renderer, time, motionBuffer) {
    this.shader.uniforms.time.value = time
    this.shader.uniforms.motionBuffer.value = motionBuffer

    renderer.setRenderTarget(null)
    renderer.render( this.scene, this.camera )
}


function resize(windowDimensions) {
    this.camera.left   = - windowDimensions.x / 2
    this.camera.right  =   windowDimensions.x / 2
    this.camera.top    =   windowDimensions.y / 2
    this.camera.bottom = - windowDimensions.y / 2

    this.camera.updateProjectionMatrix()
    this.quad.scale.set( windowDimensions.x, windowDimensions.y, 1 )
    this.shader.uniforms.dimensions.value.set( windowDimensions.x, windowDimensions.y )
}