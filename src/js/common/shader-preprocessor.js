
const INDICATOR = "@"

export function prepare(shaderCode, variablesMap) {

    variablesMap.forEach((value, key) => {
        let re = new RegExp(`${INDICATOR}${key}`, 'g');
        shaderCode = shaderCode.replace(re, value);
    })
    return shaderCode
}