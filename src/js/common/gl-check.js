import * as THREE from 'three'

/**
    Tests if rendering to float render targets is available
    THREE.FloatType not available on ios
**/
export function getFloatType(renderer) {
    const scene = new THREE.Scene()
    const camera = new THREE.OrthographicCamera( -1, 1, -1, 1, -1, 1 ) 
    const renderTarget = new THREE.WebGLRenderTarget(16, 16, {
        format: THREE.RGBAFormat,
        type: THREE.FloatType
    })
    renderer.setRenderTarget(renderTarget)
    renderer.render(scene, camera)
    const gl = renderer.getContext()
    const status = gl.checkFramebufferStatus(gl.FRAMEBUFFER)
    if (status !== gl.FRAMEBUFFER_COMPLETE) {
        console.log('FloatType not supported')
        return THREE.HalfFloatType
    }
    return THREE.FloatType
}