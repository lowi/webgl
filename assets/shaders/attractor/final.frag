precision highp float;

uniform sampler2D motionBuffer;
uniform vec2 dimensions;
uniform float time;

varying vec2 vUv;

// @NUM_SPHERES is a variable that must be replaced with a real value before loading
#define NUM_SPHERES @NUM_SPHERES

float random(vec2 n, float offset ){
	return .5 - fract(sin(dot(n.xy + vec2( offset, 0. ), vec2(12.9898, 78.233)))* 43758.5453);
}

void main() {

	vec4 motion = texture2D( motionBuffer, vUv );

	vec2 inc = motion.xy / dimensions;

	vec2 sum = motion.ba;

	// vec3 color = mix( vec3( 239., 218., 164. ) / 255., vec3( 153., 147., 113. ) / 255., motion.b );
	vec3 color = mix( vec3( 0., 0., 0. ) / 255., vec3( 240., 200., 120. ) / 255., motion.b );
	float v = clamp( length( motion.xy ), 0., 1. );
	color = mix( vec3( 0. ), color, v );
	color *= sum.x;

	// noise
	color += vec3( .25 * random( vUv, .00001 * time ) );

	float boost = 1.2;
	float reduction = 1.6;
	vec2 center = vec2( 0.5 );
	float vignette = length( vUv - .5 );
   	vignette = boost - vignette * reduction;

    color *= vignette;

	gl_FragColor = vec4( color, sum.y );

}