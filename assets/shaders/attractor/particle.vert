precision highp float;

attribute vec3 position;
attribute vec2 uv;
attribute vec2 id;

uniform mat4 modelMatrix;
uniform mat4 modelViewMatrix;
uniform mat4 projectionMatrix;
uniform mat4 prevModelViewMatrix;
uniform mat4 prevProjectionMatrix;

uniform vec2 dimensions;
uniform float size;
uniform vec3 cameraPosition;

uniform sampler2D infoTexture;
uniform sampler2D originalTexture;
uniform sampler2D positionTexture;
uniform sampler2D previousPositionTexture;
uniform sampler2D velocityTexture;

varying vec2 vUv;
varying float life;
varying vec2 vSpeed;
varying float vSize;

float modI(float a,float b) {
	float m=a-floor((a+0.5)/b)*b;
	return floor(m+0.5);
}

const float PI = 3.14159265359;

void main() {

	float ptr = id.x;
	
	// calculate the uv coordinates from the pointer-index and the dimensions 
	float u = modI( ptr, dimensions.x ) / dimensions.x;
	float v = ( ptr / dimensions.x ) / dimensions.y;
	vec2 puv = vec2( u, v );

	vUv = uv;

	// read the velocity
	// vec4 velocity = texture2D( velocityTexture, puv );
	vec4 velocity = vec4(1.);

	// read the position-information
	vec4 c = texture2D( positionTexture, puv );


	vec3 p = c.xyz * 20.;

	// update the life-span
	// life = 1. - ( c.a / 1000. );
	// if( c.a == 0. ) life = 0.;
	life = 1.;

	// ??
	vec4 modified = modelViewMatrix * vec4( p, 1. );
	
	// update the appearent size 
	modified.xyz += size  * vec3( position.xy, 0. );

	// vSpeed is a varying based on which the fragment shader calculates its brightness
	vSpeed = .1 * ( projectionMatrix * modelViewMatrix * vec4( velocity.xyz, 1. ) ).xy;

	
	gl_Position = projectionMatrix * modified;

}