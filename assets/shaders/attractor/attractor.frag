// https://www.michelemorrone.eu/glchaosp/dtAttractors.html#Lorenz

precision highp float;

uniform sampler2D origin;
uniform sampler2D positions;
uniform float dt;

varying vec2 vUv;

// De Quan Li
float a = 40.;
float c = 11.0/6.0;
float d = 0.16;
float e = 0.65;
float k = 55.;
float f = 20.;

// pNew.z = p.z + dt*(p.x * p.y - k[2] * p.z);
vec3 DeQuanLi(vec3 r) {
	//update r position
    float x = (a * (r.y - r.x) + d * r.x * r.z);
    float y = (k * r.x + f * r.y - r.x * r.z);
    float z = (c * r.z + r.x * r.y - e * pow(r.x, 2.));
  	return vec3( x, y, z );
}  



void main() {

	vec4 position = texture2D( positions, vUv );

	// hack because I can't get the initialz state to work properly
	// the workaround is to pass the origin and check the .a property for initialization
	if(position.a == 0.) { position = texture2D( origin, vUv );} 


	vec3 r = position.xyz;
	for(int i = 0; i < 5; i++) {
	 	r = r + DeQuanLi(r + DeQuanLi(r) * dt * .5) * dt;
	}

	// render
	gl_FragColor = vec4(r, 1.);
}
