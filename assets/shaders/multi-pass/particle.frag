precision highp float;

varying float vSize;
varying vec2 vUv;
varying float life;
varying vec2 vSpeed;

uniform float useTriangles;

void main() {

	// blur the triangles into circular shapes:
	// ————————————————————————————————————————
	// vec2 barycenter = vec2( .5, .6666 );
	// float d = smoothstep( .5, .55, 1. - 2. * length( vUv - barycenter ) );
	// if( d <= 0. ) discard;
	// gl_FragColor = vec4( vSpeed, life, d * life );

	gl_FragColor = vec4( vSpeed, life, life );
}
