precision highp float;

// origin
uniform sampler2D originalTexture;

// variation
uniform sampler2D infoTexture;
uniform float spread;

// simulation result
uniform sampler2D inputTexture;

// current velocity
uniform sampler2D velocityTexture;

varying vec2 vUv;

void main() {

	vec4 c = texture2D( inputTexture, vUv );

	float variation = texture2D( infoTexture, vUv ).x;

	// the base origin of the particle
	vec4 origin = texture2D( originalTexture, vUv );
	
	// respawn after the particle has died
	if( c.a == 0. ) {
		c = origin * vec4( spread, 1., spread, 1. );
	}

	// some stepsize
	float delta = 1.;

	// get the current velocity
	vec4 velocity = texture2D( velocityTexture, vUv );
	
	// update the position based on the current velocity
	c.xyz += delta * variation * velocity.xyz;

	// update the alpha
	// (I think this one is about the particle-decay)
	c.a += delta * variation;
	if( c.a > 1000. ) c.a = 0.;

	// render
	gl_FragColor = c;
}
