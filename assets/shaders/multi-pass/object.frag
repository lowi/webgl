precision highp float;

varying vec3 vPosition;
varying vec3 vNormal;

void main() {

	float rim = max( 0., abs( dot( normalize( vNormal ), normalize( -vPosition ) ) ) );
	float r = .1 * smoothstep( .25, .75, 1. - rim );
	vec3 c = vec3( 149., 200., 233. ) / 255.;

	float h = ( vPosition.y + 500. ) / 2000.;
	gl_FragColor = vec4( c * ( r * h ), 1. );

}