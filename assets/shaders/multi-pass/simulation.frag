
// precision mediump float;

// ping pong framebuffers
uniform sampler2DRect particles0;
uniform sampler2DRect particles1;

uniform float timestep;

attribute vec2 texCoordVarying;



// Lorenz Attractor parameters
float a = 10.0;
float b = 28.0;
float c = 2.6666666667;


void main(){
    int id = int(texCoordVarying.s) + int(texCoordVarying.t)*int(textureSize(particles0).x);
    vec3 pos = texture(particles0, texCoordVarying.st).xyz;
    vec3 vel = texture(particles1, texCoordVarying.st).xyz;

    // Previous positions
    float x = pos.x;
    float y = pos.y;
    float z = pos.z;
    
   	// Increments calculation
    float dx = (a * (y - x))   * timestep;
    float dy = (x * (b-z) - y) * timestep;
    float dz = (x*y - c*z)     * timestep;
    
	// Add the new increments to the previous position
    vec3 attractorForce = vec3(dx, dy, dz) ;
    pos += attractorForce;

    
    
    posOut = vec4(pos, id);
    velOut = vec4(vel, 0.0);

    gl_FragColor = vec4(position, velocity);
}