precision highp float;


// the input texture gets written by the velocity-gpu-simulation 
uniform sampler2D inputTexture;

// some variations between the pixels 
uniform sampler2D infoTexture;

// the current position
uniform sampler2D positionTexture;

// the original positions (required for respawn)
uniform sampler2D originalTexture;

uniform float time;

uniform float bounce;
uniform float friction;
uniform float gravity;
uniform float pressure;

varying vec2 vUv;

const float PI = 3.14159265359;

// float random(vec2 co){
//     return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
// }

float hash( float n ) { // 0 - 1
    return fract(sin(n)*3538.5453);
}

vec3 randomSphereDir(vec2 rnd) {

	float s = rnd.x*PI*2.;
	float t = rnd.y*2.-1.;
	return vec3(sin(s), cos(s), t) / sqrt(1.0 + t * t);

}

vec3 randomHemisphereDir(vec3 dir, float i) {
	vec3 v = randomSphereDir( vec2(hash(i+1.), hash(i+2.)) );
	return v * sign(dot(v, dir));
}

void main() {

	// variation
	// remains constant througout
	vec4 info = texture2D( infoTexture, vUv );
	
	// simulation
	// gets written to during gpuSimulation.update()
	// content is the simulation-result from previous loop
	vec4 c = texture2D( inputTexture, vUv );
	
	// position 
	// gets written to during simulation.update()
	// content is the position result from the previous loop
	vec4 p = texture2D( positionTexture, vUv );

	//	    ____
	//    ,'   Y`.
	//   /        \
	//   \ ()  () /
	//    `. /\ ,'
	//8====| "" |====8
	//     `DEAD'
	if( p.a == 0. ) {
		// reset
		vec4 origin = texture2D( originalTexture, vUv );
		c.xyz = normalize( origin.xyz );
		c.y = 0.;
	}

	// downwards motion
	c.y -= .1 * pressure;
	c.y -= .1 * info.x * gravity;
	c *= .99;

	// bounce on the 'floor'
	if( ( p.y + c.y ) < -500. ) {
		vec3 n = vec3( 0., 1., 0. );
		vec3 i = normalize( c.xyz );
		float f = length( c.xyz );
		c.xyz = f * randomHemisphereDir( reflect( i, n ), hash( length( c ) + time ) );
		c.xz *= bounce;
		c.xyz *= friction;
	}

	// render
	gl_FragColor = c;
}