precision highp float;

attribute vec3 position;
attribute vec3 normal;

uniform mat4 modelViewMatrix;
uniform mat4 projectionMatrix;
uniform mat3 normalMatrix;

varying vec3 vPosition;
varying vec3 vNormal;

void main() {

	vec4 pos = modelViewMatrix * vec4( position, 1.0 );
	vPosition = pos.xyz;
	vNormal = normalMatrix * normal;

	gl_Position = projectionMatrix * pos;

}